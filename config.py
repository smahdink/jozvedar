from os import getenv

class Config:
    """Set flask config variables"""

    # limit request size to 20 MB
    MAX_CONTENT_LENGTH = 20 * 1024 * 1024

    SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}/{databasename}".format(
    username=getenv("DB_USERNAME"),
    password=getenv("DB_PASS"),
    hostname=getenv("DB_HOST"),
    databasename=getenv("DB_DBNAME"),
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_POOL_RECYCLE = 299

    SECRET_KEY = getenv("SECRET_KEY")

class DevConfig:
    """Set flask config variables"""

    # limit request size to 20 MB
    MAX_CONTENT_LENGTH = 2 * 1024 * 1024

    SQLALCHEMY_DATABASE_URI = "sqlite:///jozvedar.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SECRET_KEY = getenv("SECRET_KEY")