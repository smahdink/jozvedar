function sort_function()
{
    let select = document.getElementById("sort_select");
    var hashtag = select.value;
    let list_items = document.getElementsByClassName("mainlist");
    
    if (list_items.length != 0)
    {
        for (i = 0; i < list_items.length; i++) {
            list_items[i].parentElement.style.display='block';
        }
    }

    if (hashtag == 'none') {
        return true
    }

    if (list_items.length != 0)
    {
        for (i = 0; i < list_items.length; i++) {
            if (!list_items[i].innerText.includes(`#${hashtag}`))
            {
                list_items[i].parentElement.style.display='none';
            }
            
        }
    }
}

let select = document.getElementById("sort_select");
let list_items = document.getElementsByClassName("mainlist");
var hashtag_list = [];

for (i = 0; i < list_items.length; i++) {
    if (list_items[i].innerText.includes("#")) {
        var tags_list = list_items[i].innerText.match(/#[\p{L}]+[_\p{L}]+/ugi)
        for (j = 0; j < tags_list.length; j++) {
            hashtag_list.push(tags_list[j].slice(1))
        }
    }
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

let unique_hashtags = hashtag_list.filter(onlyUnique)

for (k = 0; k < unique_hashtags.length; k++) {
    let option = document.createElement("option");
    option.text = unique_hashtags[k];
    option.value = unique_hashtags[k];
    select.add(option);
}
select.addEventListener("change", sort_function);