from flask import Flask, request, render_template, redirect, url_for, escape, abort
from flask_sqlalchemy import SQLAlchemy
from flask_login import current_user, login_required, login_user, LoginManager, logout_user, UserMixin
from flask_migrate import Migrate
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import StringField, PasswordField, SelectField, TextAreaField, BooleanField
from wtforms.validators import InputRequired, Length, Email
from wtforms.widgets import TextArea
from werkzeug.security import check_password_hash, generate_password_hash
from os import getenv, path
from dotenv import load_dotenv
import datetime
import dropbox
import zipfile
import magic
import requests
from unidecode import unidecode

project_folder = path.expanduser('~/projects/jozvedar')  # adjust as appropriate
load_dotenv(path.join(project_folder, '.env'))

app=Flask(__name__)
#app.config.from_object('config.Config')
app.config.from_object('config.DevConfig')


# file validation functions
def isdir(z, name):
    return any(x.startswith("%s/" % name.rstrip("/")) for x in z.namelist())
def isValidDocx(file):
    try:
        f = zipfile.ZipFile(file)
    except zipfile.BadZipFile:
        return False
    return isdir(f, "word") and isdir(f, "docProps") and isdir(f, "_rels")

# Instantiate dropbox
dbx = dropbox.Dropbox(oauth2_refresh_token=getenv("DROPBOX_REFRESH_TOKEN"), app_key=getenv("DROPBOX_APP_KEY"), app_secret=getenv("DROPBOX_APP_SECRET"))

# flask_login configs
login_manager = LoginManager()
login_manager.init_app(app)

db = SQLAlchemy(app)
migrate = Migrate(app, db, compare_type=True)

def creating_shared_link(path):
    link = dbx.sharing_create_shared_link_with_settings(path)
    return link.url

def captcha_verify(token):
    """verify the hcaptcha"""
    secret_key = getenv("HCAPTCHA_SECRET_KEY")
    url = "https://hcaptcha.com/siteverify"
    data = {"secret" : secret_key, "response" : token}
    try:
        response = requests.post(url, data=data)
    except:
        return False
    response_json = response.json()
    return response_json.get("success")


# file database model
class File(db.Model):
    __tablename__ = "files"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    file_name = db.Column(db.String(300))
    description = db.Column(db.String(300))
    file_type = db.Column(db.String(5))
    is_directory = db.Column(db.Boolean, nullable=False)
    parent_id = db.Column(db.Integer, db.ForeignKey('files.id'), nullable=True)
    parent = db.relationship('File', foreign_keys=parent_id)
    created = db.Column(db.DateTime)
    updated = db.Column(db.DateTime)
    owner_id = db.Column(db.String(10), db.ForeignKey('users.student_id'))
    link = db.Column(db.String(300))
    requests = db.relationship("Request", backref="target")

# function to secure user submitted file names. This will remove dots from file name
def secure_file_name(filename):
    def secure_char(c):
        if c.isalnum():
            return c
        else:
            return '_'
    return "".join(secure_char(c) for c in filename).rstrip('_')

# requests database model
class Request(db.Model):
    __tablename__ = "requests"
    
    id = db.Column(db.Integer, primary_key=True)
    target_id = db.Column(db.Integer, db.ForeignKey('files.id'), nullable=False)
    link = db.Column(db.String(200))
    creator_id = db.Column(db.String(10), db.ForeignKey("users.student_id"))
    created = db.Column(db.DateTime)
    description = db.Column(db.String(300), nullable=False)
    reviewed = db.Column(db.Boolean, nullable=False)


# user database model
class User(UserMixin, db.Model):
    """User database model"""
    __tablename__ = "users"

    student_id = db.Column(db.String(10), nullable = False, unique=True, primary_key=True)
    name = db.Column(db.String(128), nullable = False)
    mail = db.Column(db.String(128), nullable = False)
    password_hash = db.Column(db.String(128), nullable = False)
    files = db.relationship("File", backref='owner')
    is_main_admin = db.Column(db.Boolean)
    requests = db.relationship("Request", backref="creator")

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    def get_id(self):
        return self.student_id

@login_manager.user_loader
def load_user(user_id):
    return User.query.filter_by(student_id = user_id).first()


# form class
class Form(FlaskForm):
    student_id = StringField("شماره دانشجویی:", validators=[InputRequired("این مورد ضروری است"), Length(min=10, max=10, message="شماره دانشجویی صحیح نیست")])
    password = PasswordField("رمز عبور:", validators=[InputRequired("این مورد ضروری است")])
    remember = BooleanField("مرا به خاطر بسپار")

# sign up class
class SignupForm(Form):
    name = StringField("نام کامل:", validators=[InputRequired("مورد ضروری است"), Length(min=5, max=128, message="نام باید بین ۵ تا ۱۲۸ کاراکتر باشد")])
    mail = StringField("ایمیل:", validators=[InputRequired("این مورد ضروری است"), Email("ایمیل باید معتبر باشد")])
    password = PasswordField("رمز عبور:", validators=[InputRequired("این مورد ضروری است"), Length(min=8, max=20, message="گذرواژه باید بین ۸ تا ۲۰ کاراکتر باشد")])
    password_confirm = PasswordField("تأیید رمز عبور:", validators=[InputRequired("این مورد ضروری است")])

# new_lesson form wtf class
class NewLesson(FlaskForm):
    name = StringField("نام درس:", validators=[InputRequired("این مورد ضروری است"), Length(max=128, min=1, message="نام بلند‌تر از حد قابل قبول است")])
    owner = SelectField("مدیر", validators=[InputRequired("این مورد ضروری است")])
    description = TextAreaField("توضیحات:", validators=[Length(max=300, message="حداکثر ۳۰۰ کاراکتر!")])

# new file form class
class NewFile(FlaskForm):
    docx = FileField("فایل ورد", validators=[FileAllowed(["docx"], "فقط فایل واژه پرداز قبول می‌شود")])
    pdf = FileField("فایل pdf", validators=[FileRequired("این مورد ضروری است"), FileAllowed(["pdf"], "فقط فایل pdf قبول می‌شود")])

# new request form
class NewRequestForm(FlaskForm):
    docx = FileField("فایل واژه‌پرداز(حاوی تغییر پیشنهادی)", validators=[FileAllowed(["docx"], "فقط فایل با فرمت docx قابل قبول است")])
    description = TextAreaField("توضیح کامل تغییرات", validators=[InputRequired("این مورد ضروری است"), Length(min=20,max=300,message="بین ۲۰ تا ۳۰۰ کاراکتر")])

# deleteall form
class DeleteAll(FlaskForm):
    password = PasswordField("رمز عبور:", validators=[InputRequired("این مورد ضروری است")])

# pass change form
class ChangePass(FlaskForm):
    old_password = PasswordField("رمز فعلی:", validators=[InputRequired("این مورد ضروری است")])
    new_password = PasswordField("رمز جدید:", validators=[InputRequired("این مورد ضروری است"), Length(min=8, max=20, message="گذرواژه باید بین ۸ تا ۲۰ کاراکتر باشد")])
    confirm_password = PasswordField("تأیید رمز جدید:", validators=[InputRequired("این مورد ضروری است")])

# change owner form class
class ChangeOwner(FlaskForm):
    owner = SelectField("مدیر جدید:", validators=[InputRequired("این مورد ضروری است")])

# rename form
class Rename(FlaskForm):
    new_name = StringField("نام جدید", validators=[InputRequired("این مورد ضروری است"), Length(max=128, min=1, message="نام بلند‌تر از حد قابل قبول است")])

def is_main_admin():
    """Function to check current users permission for index page"""
    if current_user.is_authenticated:
        with open(path.join(project_folder, 'admins'), "r") as f:
            admins = f.read().split(',')
        if current_user.student_id in admins:
            return True
    return False

def get_message_of_day():
    try:
        with open(path.join(project_folder, 'MessageOfDay'), 'r') as f:
            return f.read()
    except FileNotFoundError:
        return ''

def set_message_of_day(msg):
    with open(path.join(project_folder, 'MessageOfDay'), 'w') as f:
        f.write(msg)


app.jinja_env.globals.update(get_message_of_day=get_message_of_day)

@app.route("/")
def index():
    # render a list of available lessons
    lessons = []
    for f in File.query.filter_by(is_directory=True, parent_id=None):
        lessons.append(f)
    
    return render_template("index.html", is_admin = is_main_admin(), lessons=lessons)

@app.route("/login", methods=["GET", "POST"])
def login():
    form = Form() # Instanciate a form
    if request.method == "GET":
        return render_template("login.html", form=form, error=False, error_msg=None)

    if not captcha_verify(request.form.get("h-captcha-response")):
        return render_template("login.html", form=form, error=True, error_msg="کپچا تأیید نشد لطفا دوباره امتحان کنید")

    # Validate form
    if not form.validate_on_submit():
        return render_template("login.html", form=form, error=False, error_msg=None)

    user = load_user(unidecode(form.student_id.data))
    if user is None:
        return render_template("login.html", form=form, error=True, error_msg="اطلاعات ورود صحیح نیست")
    if not user.check_password(form.password.data):
        return render_template("login.html", form=form, error=True, error_msg="اطلاعات ورود صحیح نیست")
    login_user(user, remember=True if form.remember.data else False, duration=datetime.timedelta(7))
    return redirect(url_for("index"))

@app.route("/signup", methods=["GET", "POST"])
def signup():
    if getenv("DISABLE_SIGNUP") == 'true':
        return redirect(url_for("index"))
    form = SignupForm()
    if request.method == "GET":
        return render_template("signup.html", form=form, error=False, error_msg=None)
    
    if not captcha_verify(request.form.get("h-captcha-response")):
        return render_template("signup.html", form=form, error=True, error_msg="کپچا تأیید نشد لطفا دوباره امتحان کنید")
    
    # Validate form
    if not form.validate_on_submit():
        return render_template("signup.html", form=form, error=False, error_msg=None)

    # validate student_id
    for i in form.student_id.data:
        if not i.isdigit():
            return render_template("signup.html", error=True, error_msg="شماره دانشجویی نامعتبر است", form=form)
    student_id = unidecode(form.student_id.data)
    name = escape(form.name.data.strip())
    mail = escape(form.mail.data.strip())
    password = form.password.data

    if form.password.data != form.password_confirm.data:
        return render_template("signup.html", error=True, error_msg="گذرواژه‌های وارد شده متفاوت اند.", form=form)

    user = User.query.filter_by(student_id=student_id).first() # if this returns a user, then the email already exists in database

    if user: # if a user is found, we want to redirect back to signup page so user can try again
        return render_template("signup.html", error=True, error_msg="این شمارهٔ دانشجویی قبلاً ثبت شده است. در صورت بروز اشتباه با نماینده در میان بگذارید.", form=form)

    # create a new user with the form data. Hash the password so the plaintext version isn't saved.
    new_user = User(student_id=student_id, name=name,mail=mail, password_hash=generate_password_hash(password))

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()

    login_user(new_user)
    return redirect(url_for("index"))

@app.route("/logout")
def logout():
    if not current_user.is_authenticated:
        return redirect(url_for("index"))
    logout_user()
    return redirect(url_for("index"))

# function for admin to create a new lesson
@app.route("/create_lesson", methods=["GET", "POST"])
def create_lesson():
    # check permission
    if not is_main_admin():
        return redirect(url_for("index"))
    
    # instantiate form and validate it
    form = NewLesson()
    form.owner.choices=[(user.student_id, user.name + "-" + str(user.student_id)) for user in User.query.all()]
    if request.method == "GET":
        return render_template("create_lesson.html", form=form, error=False, error_msg=None)
    if not form.validate_on_submit():
        return render_template("create_lesson.html", form=form, error=False, error_msg=None)
    
    name = escape(form.name.data.strip())
    if len(File.query.filter_by(name=name, file_type='l').all()) != 0:
        return render_template("create_lesson.html", form=form, error=True, error_msg="جلسه‌ دیگری با همین نام وجود دارد. لطفاً نام دیگری انتخاب کنید.")
    if form.description.data != None:
        description = escape(form.description.data.strip())
    else:
        description = None
    is_directory = True
    parent_id = None
    created = datetime.datetime.now()
    updated = None
    owner = form.owner.data
    file_type = 'l'

    new_lesson = File(name=name, description=description, is_directory=is_directory, parent_id=parent_id, created=created, updated=updated, owner=load_user(owner), file_type=file_type)
    db.session.add(new_lesson)
    db.session.commit()
    
    return redirect("/show/{}".format(new_lesson.id))

# Function to return lesson or session
@app.route("/show/<string:id>")
def show(id):
    requested_file = File.query.filter_by(id=id).first()
    if requested_file == None:
        return redirect(url_for("index"))
    # load child files
    children = File.query.filter_by(parent_id=requested_file.id).all()
    
    # check if requested file is a lesson
    if requested_file.file_type == "l":
        if current_user == requested_file.owner or is_main_admin():
            is_admin = True
        else:
            is_admin = False
        return render_template("lesson.html", requested_file=requested_file, child_files=children, is_admin=is_admin)
    
    # check if requested file is a session
    if requested_file.file_type == "s":
        parent = File.query.filter_by(id=requested_file.parent_id).first()
        if current_user == requested_file.owner or current_user == parent.owner or is_main_admin():
            is_admin = True
        else:
            is_admin = False
        if len(children) == 2:
            if children[0].file_type == "word" and children[1].file_type == "pdf":
                word_link = children[0].link[0:-1] + '1'
                pdf_link = children[1].link[0:-1] + '1'
            else:
                word_link = children[1].link[0:-1] + '1'
                pdf_link = children[0].link[0:-1] + '1'
        elif len(children) == 1:
            pdf_link = children[0].link[0:-1] + '1'
            word_link = None
        else:
            word_link = None
            pdf_link = None
        
        # get requests for this file
        requests = requested_file.requests

        return render_template("session.html", requested_file=requested_file, docx_link=word_link, pdf_link=pdf_link, requests=requests, is_admin=is_admin)
    
    # if requested file is an actual file
    return redirect("/show/{}".format(requested_file.parent_id))

@app.route("/create_session/<string:id>", methods=["GET", "POST"])
def create_session(id):
    if not current_user.is_authenticated:
        return redirect(url_for('login'))

    lesson = File.query.filter_by(id=id).first()
    if lesson == None:
        return redirect(url_for("index"))
    if current_user != lesson.owner:
        if not is_main_admin():
            return redirect("/show/{}".format(id))
    if lesson.file_type != "l":
        return redirect(url_for("index"))

    # instanciate form
    form = NewLesson()
    form.owner.choices=[(user.student_id, user.name + "-" + str(user.student_id)) for user in User.query.all()]

    if request.method == "GET":
        return render_template("create_session.html", form=form, parent=lesson, error=False, error_msg=None)
    
    
    if not form.validate_on_submit():
        return render_template("create_session.html", form=form, parent=lesson, error=False, error_msg=None)

    name = escape(form.name.data.strip())
    if len(name) == 0:
        return render_template("create_session.html", form=form, parent=lesson, error=True, error_msg="نام جلسه نباید خالی باشد")
    if len(File.query.filter_by(name=name, parent_id=lesson.id).all()) != 0:
        return render_template("create_session.html", form=form, parent=lesson, error=True, error_msg="جلسه‌ای با همین نام در این درس وجود دارد. لطفاً نام دیگری انتخاب کنید.")
    is_directory = True
    parent = lesson
    file_type = 's'
    created = datetime.datetime.now()
    owner = form.owner.data

    new_session = File(name=name, is_directory=is_directory, parent_id=parent.id, created=created, owner=load_user(owner), file_type=file_type)
    db.session.add(new_session)
    db.session.commit()

    # Redirect to new session
    return redirect("/show/{}".format(new_session.id))

@app.route("/new_file/<string:id>", methods=["GET", "POST"])
def new_file(id):
    if not current_user.is_authenticated:
        return redirect(url_for('login'))
    # Session owner can upload new files with this function
    session = File.query.filter_by(id=id).first()
    session_parent = File.query.filter_by(id=session.parent_id).first()
    childs = File.query.filter_by(parent_id=session.id).all()
    if session == None:
        return redirect(url_for("index"))
    if current_user != session.owner:
        if not is_main_admin():
            if current_user != session_parent.owner:
                return redirect("/show/{}".format(id))
    if session.file_type != 's':
        return redirect(url_for("index"))
    
    form = NewFile()

    if request.method == "GET":
        return render_template("newfile.html", form=form, session=session, error=False, error_msg=None)
    
    if not captcha_verify(request.form.get("h-captcha-response")):
        return render_template("newfile.html", form=form, session=session, error=True, error_msg="کپچا تأیید نشد لطفا دوباره امتحان کنید")
    
    if not form.validate_on_submit():
        return render_template("newfile.html", form=form, session=session, error=False, error_msg=None)

    # Get and validate files
    pdf = form.pdf.data
    docx = form.docx.data
    
    if docx :
        if not isValidDocx(docx):
            if "Microsoft Word" not in magic.from_buffer(docx.read(2048)):
                return render_template("newfile.html", form=form, session=session, error=True, error_msg="فایل واژه‌پرداز معتبر نیست")
        docx.seek(0)
        
    
    if "PDF document" not in magic.from_buffer(pdf.read(2048)):
        return render_template("newfile.html", form=form, session=session, error=True, error_msg="فایل pdf معتبر نیست")
    pdf.seek(0)


    name = None
    is_directory = False
    updated = datetime.datetime.now()
    parent = session
    docx_filename = str(session.id) + ".docx"
    pdf_filename =  str(session.id) + ".pdf"
    if request.form.get("textbox") != None:
        description = escape(request.form.get("textbox").strip())
        if len(description) >= 300:
            return render_template("newfile.html", form=form, session=session, error=True, error_msg="توضیحات نباید بیش از ۳۰۰ حرف باشد")
    else:
        description = None

    # determine if the file is updating or newly updloaded
    if len(File.query.filter_by(parent_id=session.id).all()) >= 1:
        # updating a file
        try:
            dbx.files_upload(pdf.read(), "/files/{}/{}".format(session_parent.name ,pdf_filename), mode=dropbox.files.WriteMode.overwrite)
            if docx:
                dbx.files_upload(docx.read(), "/files/{}/{}".format(session_parent.name ,docx_filename), mode=dropbox.files.WriteMode.overwrite)
        except dropbox.exceptions.ApiError:
            return "api error"
        # add word if newly uploaded
        if len(File.query.filter_by(parent_id=session.id).all()) == 1:
            new_word = File(name=name, is_directory=is_directory, created=updated, updated=updated, parent_id=parent.id, file_name=docx_filename, file_type="word", link=creating_shared_link("/files/{}/{}".format(session_parent.name, docx_filename)))
            db.session.add(new_word)
        # update the modified column
        File.query.filter_by(id=session.id).update({"updated":updated, "description":description})
        db.session.commit()
    else:
        # uploading for the first time
        try:
            if docx:
                dbx.files_upload(docx.read(), "/files/{}/{}".format(session_parent.name, docx_filename))
            dbx.files_upload(pdf.read(), "/files/{}/{}".format(session_parent.name, pdf_filename))
        except dropbox.exceptions.ApiError:
            return "api error"
        
        # adding files to database
        new_pdf = File(name=name, is_directory=is_directory, created=updated, updated=updated, parent_id=parent.id, file_name=pdf_filename, file_type="pdf", link=creating_shared_link("/files/{}/{}".format(session_parent.name, pdf_filename)))
        if docx:
            new_word = File(name=name, is_directory=is_directory, created=updated, updated=updated, parent_id=parent.id, file_name=docx_filename, file_type="word", link=creating_shared_link("/files/{}/{}".format(session_parent.name, docx_filename)))
        File.query.filter_by(id=session.id).update({"updated":updated, "description":description})
        db.session.add(new_pdf)
        if docx:
            db.session.add(new_word)
        db.session.commit()
        return redirect("/show/{}".format(id))

    return redirect("/show/{}".format(id))

@app.route("/new_request/<string:id>", methods=["GET", "POST"])
def new_request(id):
    # creating new requests
    if not current_user.is_authenticated:
        return redirect(url_for("login"))
    
    # load the desired session
    session = File.query.filter_by(id=id).first()
    if session == None:
        return redirect("/show/{}".format(id))
    if session.file_type != 's':
        return redirect("/show/{}".format(id))

    # instantiate form
    form = NewRequestForm()

    if request.method == "GET":
        return render_template("new_request.html", form=form, session=session, error=False, error_msg=None)
    
    if not captcha_verify(request.form.get("h-captcha-response")):
        return render_template("new_request.html", form=form, session=session, error=True, error_msg="کپچا تأیید نشد لطفا دوباره امتحان کنید")

    if not form.validate_on_submit():
        return render_template("new_request.html", form=form, session=session, error=False, error_msg=None)

    description = escape(form.description.data.strip())
    if len(description) < 20:
        return render_template("new_request.html", form=form, session=session, error=True, error_msg="توضیحات باید حداقل ۲۰ کاراکتر باشد")
    
    created = datetime.datetime.now()

    new_request = Request(creator=current_user, target=session, created=created, description=description, reviewed=False)

    # adding the entry to db to get its id for filename
    db.session.add(new_request)
    db.session.commit()

    # get and validate docx file if available
    docx = form.docx.data
    if docx != None:
        if not isValidDocx(docx):
            if "Microsoft Word" not in magic.from_buffer(docx.read(2048)):
                return render_template("new_request.html", form=form, session=session, error=True, error_msg="فایل واژه‌پرداز معتبر نیست")
            docx.seek(0)
        try:
            dbx.files_upload(docx.read(), "/requests/{}.docx".format(new_request.id))
            link = creating_shared_link("/requests/{}.docx".format(new_request.id))
        except dropbox.exceptions.ApiError:
            db.session.delete(new_request)
            db.session.commit()
            return render_template("new_request.html", form=form, session=session, error=True, error_msg="مشکلی پیش آمده است. لطفا بعداً دوباره امتحان کنید و در صورت پایداری مشکل به نماینده اطلاع دهید.")

        # adding link to db entry
        new_request.link = link
        db.session.commit()
    
    return redirect("/show/{}".format(id))

@app.route("/review/<string:id>", methods=["GET"])
def review(id):
    request = Request.query.filter_by(id=id).first()
    if request == None:
        abort(404)
    target = File.query.filter_by(id=request.target_id).first()
    target_parent = File.query.filter_by(id=target.parent_id).first()
    
    # check permission
    if not current_user == target.owner:
        if not is_main_admin():
            if not current_user == target_parent.owner:
                abort(403)
    
    request.reviewed = True
    db.session.commit()
    return redirect("/show/{}".format(request.target.id))


@app.route("/deleteall", methods=["GET", "POST"])
def deleteall():
    if not is_main_admin():
        abort(403)
    form = DeleteAll()
    if request.method == "GET":
        return render_template("delete_all.html", form=form, error=False, error_msg=None)
    if not form.validate_on_submit():
        return render_template("delete_all.html", form=form, error=False, error_msg=None)
    if not current_user.check_password(form.password.data):
        return render_template("delete_all.html", form=form, error=True, error_msg="رمز صحیح نیست")
    if not captcha_verify(request.form.get("h-captcha-response")):
        return render_template("delete_all.html", form=form, error=True, error_msg="مشکل در کپچا. دوباره امتحان کنید.")
    

    # Try deleting files
    if len(Request.query.all()) != 0:
        if len(Request.query.filter_by(link=None).all()) != len(Request.query.all()): 
            try:
                dbx.files_delete("/requests")
                Request.query.delete()
            except dropbox.exceptions.ApiError:
                return render_template("delete_all.html", form=form, error=True, error_msg="مشکلی پیش آمده است لطفا بعدا امتحان کنید")
        else:
            Request.query.delete()
    
    if len(File.query.all()) != 0:
        try:
            if len(File.query.filter_by(file_type="word").all()) != 0:
                dbx.files_delete("/files")
            File.query.filter_by(file_type="pdf").delete()
            File.query.filter_by(file_type="word").delete()
            db.session.commit()
            File.query.filter_by(file_type="s").delete()
            db.session.commit()
            File.query.filter_by(file_type="l").delete()

        except dropbox.exceptions.ApiError:
            return render_template("delete_all.html", form=form, error=True, error_msg="مشکلی پیش آمده است لطفا بعدا امتحان کنید")
    
    db.session.commit()
    return redirect(url_for("index"))

@app.route("/passchange", methods=["GET", "POST"])
def changepass():
    if not current_user.is_authenticated:
        return redirect(url_for("login"))
    
    form = ChangePass()
    if request.method == "GET":
        return render_template("change_pass.html", form=form, error=False, error_msg=None)
    
    if not captcha_verify(request.form.get("h-captcha-response")):
        return render_template("change_pass.html", form=form, erorr=True, error_msg="کپچا تأیید نشد. لطفاً دوباره امتحان کنید.")
    if not form.validate_on_submit():
        return render_template("change_pass.html", form=form, error=False, error_msg=None)
    
    user = load_user(current_user.student_id)

    if not user.check_password(form.old_password.data):
        return render_template("change_pass.html", form=form, error=True, error_msg="رمز فعلی صحیح نیست.")
    
    if form.new_password.data != form.confirm_password.data:
        return render_template("change_pass.html", form=form, error=True, error_msg= "گذرواژه جدید با تأیید آن متفاوت است.")

    user.password_hash = generate_password_hash(form.new_password.data)
    db.session.commit()
    return redirect(url_for("index"))

@app.route("/changeowner/<string:id>", methods=["GET","POST"])
def changeowner(id):
    if not current_user.is_authenticated:
        return redirect(url_for('login'))
    
    subject = File.query.filter_by(id=id).first()
    if subject == None:
        abort(404)
    
    if subject.file_type == "l":
        if not is_main_admin():
            abort(403)
    elif subject.file_type == "s":
        if not is_main_admin():
            parent = File.query.filter_by(id=subject.parent_id).first()
            if current_user != parent.owner:
                abort(403)
    else:
        return redirect(url_for('index'))

    form = ChangeOwner()
    form.owner.choices=[(user.student_id, user.name + "-" + str(user.student_id)) for user in User.query.all()]
    
    if request.method == "GET":
        return render_template("change_owner.html", form=form, error=False, error_msg=None, subject=subject)
    
    if not form.validate_on_submit():
        return render_template("change_owner.html", form=form, error=False, error_msg=None, subject=subject)
    if not captcha_verify(request.form.get("h-captcha-response")):
        return render_template("change_owner.html", form=form, error=True, error_msg="کپچا تأیید نشد لطفا دوباره امتحان کنید", subject=subject)
    
    subject.owner = load_user(form.owner.data)
    db.session.commit()
    return redirect("/show/{}".format(id))

@app.route("/setmessage")
def set_message():
    if not current_user.is_authenticated:
        abort(404)
    if not current_user.student_id == getenv("MAINTAINER"):
        abort(404)
    message = escape(request.args.get("msg").strip())
    set_message_of_day(message)
    return 'success'

@app.route("/breakout")
def breakout():
    return redirect(url_for('static', filename='breakout.html'))

@app.route("/help")
def help():
    return render_template("help.html")

@app.route("/rename/<string:id>", methods=["GET", "POST"])
def rename(id):
    """Renames a lesson or a session"""
    # Login check
    if not current_user.is_authenticated:
        return redirect(url_for("login"))
    
    subject = File.query.filter_by(id=id).first()
    if subject == None:
        abort(404)
    
    if subject.file_type not in ["l", "s"]:
        abort(404)

    #permission check
    if current_user != subject.owner:
        if not is_main_admin():
            if subject.file_type == 's':
                parent = File.query.filter_by(id = subject.parent_id).first()
                if current_user != parent.owner:
                    abort(403)
            elif subject.file_type == 'l':
                if current_user != subject.owner:
                    abort(403)

    form = Rename()
    if request.method == "GET":
        return render_template("rename.html", form = form, subject = subject, error = False)
    
    if not form.validate_on_submit():
        return render_template("rename.html", form = form, subject = subject, error = False)

    # renaming database entry
    new_name = escape(form.new_name.data.strip())
    if len(File.query.filter_by(name=new_name).all()) != 0:
        return render_template("rename.html", form=form, subject = subject, error=True, error_msg="جلسه‌ دیگری با همین نام وجود دارد. لطفاً نام دیگری انتخاب کنید.")
    subject.name = new_name
    db.session.commit()
    return redirect("/show/{}".format(id))